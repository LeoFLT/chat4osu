An IRC chat for osu!

This fork is virtually identical to the original with the exception of two new commands that I use often: !mp aborttimer and !mp clearhost and styling changes.

![alt text](./preview.png "The sidebar commands are centered and made to look \(in my opinion\) better.")