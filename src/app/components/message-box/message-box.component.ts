import {
  Component,
  OnInit,
  Input,
  ViewChild,
  OnChanges,
  Output,
  EventEmitter
} from '@angular/core';
import moment from 'moment';
import { VirtualScrollerComponent } from 'ngx-virtual-scroller';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss']
})
export class MessageBoxComponent implements OnInit, OnChanges {
  @Input()
  messages: { sender: string; message: string; date: Date; action?: boolean }[];

  @Input()
  currentChannel: string;

  @Input()
  myUsername: string;

  @Output()
  openProfile = new EventEmitter<string>();

  viewPortItems: {
    sender: string;
    message: string;
    date: Date;
    action?: boolean;
  }[];

  @ViewChild('scroll', { static: true })
  private virtualScroll: VirtualScrollerComponent;

  constructor() { }

  ngOnInit(): void {
    // Scroll to bottom once messages loaded (for example when navigating from settings page)
    setTimeout(() => {
      // Dont run if the viewport hasn't been initialized
      if (!this.viewPortItems || !this.messages) {
        return;
      }

      this.scrollToBottom();
    }, 100);
  }

  ngOnChanges(): void {
    // Dont run if the viewport hasn't been initialized
    if (!this.viewPortItems || !this.messages) {
      return;
    }

    // If we just changed channel, scroll to bottom
    if (this.messages.indexOf(this.viewPortItems[0]) === -1) {
      this.scrollToBottom();
    }

    // If we're at the bottom, scroll to bottom
    if (
      this.viewPortItems[this.viewPortItems.length - 1] === this.messages[this.messages.length - 2]
    ) {
      this.scrollToBottom();
    }
  }

  parseDate(date: Date): string {
    const tempDate = moment(date);
    return `${('0' + tempDate.hours()).slice(-2)}:${('0' + tempDate.minutes()).slice(-2)}:${('0' + tempDate.seconds()).slice(-2)}`;
  }

  scrollToBottom() {
    if (this.virtualScroll) {
      this.virtualScroll.scrollToIndex(this.messages.length - 1, true, 0, 0);
    }
  }

  clickUser(username: string) {
    this.openProfile.emit(username);
  }

  includesCaseInsensitive(textToCheck: string, value: string) {
    return textToCheck.toUpperCase().includes(value.toUpperCase());
  }
}
