import {
  State,
  Action,
  StateContext,
  Selector,
  createSelector,
  Store
} from '@ngxs/store';
import produce from 'immer';
import {
  ReceiveMessage,
  SendMessage,
  SendMessageToChannel
} from '../actions/message.actions';
import { JoinChannelSuccess, LeaveChannel, ChangeChannelName } from '../actions/channel.actions';
import { ChannelState, ChannelStateModel } from './channel.state';
import { IrcService } from '../../providers/irc.service';
import { AuthState } from './auth.state';
import { Logout } from '../actions/auth.actions';
import { Injectable } from '@angular/core';

export interface MessageStateModel {
  messages: {
    [channelName: string]: {
      sender: string;
      message: string;
      date: Date;
      action?: boolean;
    }[];
  };
  history: string[];
}

@State<MessageStateModel>({
  name: 'message',
  defaults: {
    messages: {},
    history: []
  }
})
@Injectable()
export class MessageState {
  @Selector([ChannelState])
  static currentChannelMessages(
    state: MessageStateModel,
    channelState: ChannelStateModel
  ) {
    return state.messages[channelState.currentChannel];
  }

  constructor(private store: Store, private irc: IrcService) { }

  @Action(SendMessage)
  SendMessage(ctx: StateContext<MessageStateModel>, action: SendMessage) {
    const channelName = this.store.selectSnapshot(ChannelState.currentChannel);
    const username = this.store.selectSnapshot(AuthState.username);
    const message = action.payload.message.trim();
    this.irc.sendMessage(channelName, message);

    if (message.charAt(0) !== '/') {
      ctx.setState(
        produce(ctx.getState(), draft => {
          // Create the channel array if it doesn't exist yet
          if (!draft.messages[channelName]) {
            draft.messages[channelName] = [];
          }

          draft.history.push(message);

          draft.messages[channelName].push({
            message: message,
            sender: username,
            date: action.payload.date
          });
        })
      );
    }
  }

  @Action(SendMessageToChannel)
  sendMessageToChannel(
    ctx: StateContext<MessageStateModel>,
    action: SendMessageToChannel
  ) {
    const channelName = action.payload.channel;
    const username = this.store.selectSnapshot(AuthState.username);
    const message = action.payload.message.trim();
    this.irc.sendMessage(channelName, message);

    if (message.charAt(0) !== '/') {
      ctx.setState(
        produce(ctx.getState(), draft => {
          // Create the channel array if it doesn't exist yet
          if (!draft.messages[channelName]) {
            draft.messages[channelName] = [];
          }

          draft.history.push(message);

          draft.messages[channelName].push({
            message: message,
            sender: username,
            date: action.payload.date
          });
        })
      );
    }
  }

  @Action(ReceiveMessage)
  receiveMessage(ctx: StateContext<MessageStateModel>, action: ReceiveMessage) {
    const myUsername = this.store.selectSnapshot(AuthState.username);
    ctx.setState(
      produce(ctx.getState(), draft => {
        let channelKey = Object.keys(draft.messages).find(
          key => key.toLowerCase() === action.payload.channelName.toLowerCase()
        );
        console.log('channel key', channelKey);
        console.log('channel name', action.payload.channelName);
        console.log('messages', draft.messages);

        // Create the channel array if it doesn't exist yet
        if (!channelKey || !draft.messages[channelKey]) {
          draft.messages[action.payload.channelName] = [];
          channelKey = action.payload.channelName;
        }

        if (draft.messages[channelKey]) {
          draft.messages[channelKey].push({
            message: action.payload.message,
            sender: action.payload.sender,
            date: action.payload.date,
            action: action.payload.action
          });
        }

        // Highlights
        if (!action.payload.action && action.payload.message.toUpperCase().includes(myUsername.toUpperCase())) {
          this.store.dispatch(new ReceiveMessage({
            action: true,
            channelName: '#highlights',
            sender: 'Highlighted',
            message: `in ${action.payload.channelName} by ${action.payload.sender}: ${action.payload.message}`,
            date: action.payload.date
          }));
        }
      })
    );
  }

  @Action(JoinChannelSuccess)
  createChannel(
    ctx: StateContext<MessageStateModel>,
    action: JoinChannelSuccess
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        if (!draft.messages[action.payload.channelName]) {
          draft.messages[action.payload.channelName] = [];
        }
      })
    );
  }

  @Action(ChangeChannelName)
  changeChannelName(
    ctx: StateContext<MessageStateModel>,
    action: ChangeChannelName
  ) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.messages[action.payload.newName] = [...draft.messages[action.payload.channelName]];
        delete draft.messages[action.payload.channelName];
      })
    );
  }

  @Action(LeaveChannel)
  leaveChannel(ctx: StateContext<MessageStateModel>, action: LeaveChannel) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        delete draft.messages[action.payload.channelName];
      })
    );
  }

  @Action(Logout)
  async logout(ctx: StateContext<MessageStateModel>) {
    ctx.setState(
      produce(ctx.getState(), draft => {
        draft.messages = {};
        draft.history = [];
      })
    );
  }
}
